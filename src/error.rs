use reqwest::{StatusCode, Url};
use std::fmt::{Display, Formatter};
use std::io::ErrorKind;

#[derive(Debug)]
pub enum Error {
    Url(Option<Url>),
    Status(Option<StatusCode>),
    FileCreation(ErrorKind),
    GetTotalSize,
    Unknown,
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self)
    }
}

impl std::error::Error for Error {}

impl From<reqwest::Error> for Error {
    fn from(err: reqwest::Error) -> Self {
        if err.is_redirect() || err.is_request() {
            Self::Url(err.url().cloned())
        } else if err.is_status() {
            Self::Status(err.status())
        } else {
            Self::Unknown
        }
    }
}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Self {
        Self::FileCreation(err.kind())
    }
}
